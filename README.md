### Ilumno WordPress
![](https://www.ilumno.com/assets/icons/apple-icon-72x72.png)
1.	Crear un plugin llamado “testplugin”
2.	Al momento de activar el plugin se debe crear una página automáticamente con el nombre “Prueba de Wordpress” (Validar previamente que la pagina no exista), en caso de que exista deberá adicionarle un consecutivo Ejemplo: “Prueba de Wordpress 1”.
3.	En la descripción de la página se debe agregar un shortcode con el nombre “[test_add_message]”.
4.	El shortcode puede tener cualquier texto descriptivo.
5.	El slug por defecto de la página debe ser “test_worpress”, por consiguiente, la url de la página deberá ser “{site_url}/test_worpress”.
6.	Al desactivar el plugin, la página que se ha creado debe eliminarse automáticamente.


# Instalador
`wp-pluguin_ilumno.zip`

![](https://th.bing.com/th/id/R.ccd51215fd5a784acc78e158f6cd7ef9?rik=o650%2fWBWaFGbCA&pid=ImgRaw&r=0)
