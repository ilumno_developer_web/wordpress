<?php

/**
 * Plugin Name: Test Plugin
 * Plugin URI:  https://elaverde.me
 * Description: Un plugin de prueba para crear una página y un shortcode propuesta por la empresa ilumno.
 * Version:     0.0.1
 * Author:      Edilson Laverde
 * Author URI:  https://elaverde.me
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: testplugin
 */

// Si este archivo es llamado directamente, abortar.
if ( ! defined( 'WPINC' ) ) {
	die;
}
function create_page($page_title){
    $page = array(
        'post_type'    => 'page',
        'post_title'   => $page_title,
        'post_content' => '[test_add_message]', // Agregar el shortcode en la descripción
        'post_status'  => 'publish',
        'post_author'  => 1,
        'post_name' => 'test_worpress' // Establecer el slug de la página
    );
    wp_insert_post( $page );
}

// Crear la página cuando se active el plugin
function testplugin_activate() {
	// Verificar si la página ya existe
	$page_title = 'Prueba de Wordpress';
	$page_check = get_page_by_title( $page_title );
    
    $pages = get_pages( array(
        'title_like'   => $page_title.'%',
        'post_status'  => 'publish',
    ));
	// Si la página no existe, crearla
	if (  count( $pages ) == 0 ) {
		create_page($page_title);
	} else { // Si la página ya existe, agregar un consecutivo al título
		$page_title .= ' ' . ( count( $pages ));
        create_page($page_title);
	}
}
register_activation_hook( __FILE__, 'testplugin_activate' );

// Eliminar la página cuando se desactive el plugin
function testplugin_deactivate() {
	// Verificar si la página existe
	$page_title = 'Prueba de Wordpress';
    $pages = get_pages( array(
        'title_like'   => $page_title.'%',
        'post_status'  => 'publish',
    ));
    //recorremos las paginas creadas y las eliminamos
    foreach ($pages as $page) {
        wp_delete_post( $page->ID, true );
    }
}
register_deactivation_hook( __FILE__, 'testplugin_deactivate' );

//creamos el shortcode
function test_add_message_shortcode() {
    return 'Hola Mundo Ilumnu';
}
add_shortcode('test_add_message', 'test_add_message_shortcode');